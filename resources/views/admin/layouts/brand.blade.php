<div class="navbar-brand-wrapper d-flex justify-content-center">
    <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
        <a class="navbar-brand brand-logo" href="{{route('admin.dashboard')}}">
            <strong>{{$appSetting->website_name}}</strong>
        </a>
        <a class="navbar-brand brand-logo-mini" href="{{route('admin.dashboard')}}">
            <img src="{{asset('admin/images/logo-mini.svg')}}" alt="logo"/>
        </a>
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-sort-variant"></span>
        </button>
    </div>
</div>
