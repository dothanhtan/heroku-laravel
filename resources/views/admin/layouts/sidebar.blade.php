<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item {{Request::is('admin/dashboard') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('admin.dashboard')}}">
                <i class="mdi mdi-speedometer menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/categories') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('categories.index')}}">
                <i class="mdi mdi-format-list-bulleted-type menu-icon"></i>
                <span class="menu-title">Categories</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/products') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('products.index')}}">
                <i class="mdi mdi-plus-circle menu-icon"></i>
                <span class="menu-title">Products</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/brands') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('brands.index')}}">
                <i class="mdi mdi-bookmark menu-icon"></i>
                <span class="menu-title">Brands</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/colors') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('colors.index')}}">
                <i class="mdi mdi-invert-colors menu-icon"></i>
                <span class="menu-title">Colors</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/sizes') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('sizes.index')}}">
                <i class="mdi mdi-move-resize menu-icon"></i>
                <span class="menu-title">Sizes</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/orders') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('orders.index')}}">
                <i class="mdi mdi-marker-check menu-icon"></i>
                <span class="menu-title">Orders</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/users') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('users.index')}}">
                <i class="mdi mdi-account-multiple-plus menu-icon"></i>
                <span class="menu-title">Users</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/banners') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('banners.index')}}">
                <i class="mdi mdi-vibrate menu-icon"></i>
                <span class="menu-title">Banner</span>
            </a>
        </li>
        <li class="nav-item {{Request::is('admin/settings') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('settings.index')}}">
                <i class="mdi mdi-settings menu-icon"></i>
                <span class="menu-title">Setting</span>
            </a>
        </li>
    </ul>
</nav>
